vim.o.tabstop = 2 -- number of spaces for <tab>
vim.o.shiftwidth = 2 -- number of spaces for indentation
vim.o.softtabstop = 2 -- whitespace to be added
vim.o.commentstring = "# %s"
