local map = vim.keymap.set

-- required for correct translation of key strings
local function t(str)
    return vim.api.nvim_replace_termcodes(str, true, true, true)
end

-- windows
local mode_prefixes = { t = "<C-\\><C-N>", i = "<C-\\><C-N>", n = "" }
local directions = { h = "left", j = "down", k = "up", l = "right" }
for m, p in pairs(mode_prefixes) do
    for k, d in pairs(directions) do
        local kup = k:upper()
        map(m, "<A-" .. k .. ">", t(p .. "<C-w>" .. k), { desc = "select window " .. d })
        map(m, "<A-" .. kup .. ">", t(p .. "<C-w>" .. kup), { desc = "move window " .. d })
    end
end

-- resize window
map("n", "<C-Up>", "<cmd>resize +2<cr>", { desc = "Increase window height" })
map("n", "<C-Down>", "<cmd>resize -2<cr>", { desc = "Decrease window height" })
map("n", "<C-Left>", "<cmd>vertical resize -2<cr>", { desc = "Decrease window width" })
map("n", "<C-Right>", "<cmd>vertical resize +2<cr>", { desc = "Increase window width" })

-- cursor movement
map("n", "r", "gr", { desc = "[r]eplace virtual character under curser" })
map("n", "R", "gR", { desc = "enter virtual [R]eplacement mode" })
map("n", "j", "gj", { desc = "down visual line" })
map("n", "k", "gk", { desc = "up visual line" })
map("n", "gj", "j", { desc = "down actual line" })
map("n", "gk", "k", { desc = "up actual line" })
map("n", "H", "^", { desc = "move to beginning of the line" })
map("n", "L", "$", { desc = "move to end of the line" })

-- undo
map("n", "U", t("<C-R>"), { desc = "redo" })

-- splits
map("n", "<leader>|", ":vsplit<cr>", { desc = "split vertically" })
map("n", "<leader>-", ":split<cr>", { desc = "split horizontally" })

-- indentation
map("v", "<", "<gv", { desc = "shift left" })
map("v", ">", ">gv", { desc = "shift right" })

-- visual block movement
map("v", "J", t(":m >+1<CR>gv=gv"), { desc = "move visual block down" })
map("v", "K", t(":m >-2<CR>gv=gv"), { desc = "move visual block up" })

-- diagnostics
map("n", "[d", vim.diagnostic.goto_prev)
map("n", "]d", vim.diagnostic.goto_next)
map("n", "<leader>e", vim.diagnostic.open_float)
map("n", "<leader>q", vim.diagnostic.setloclist)
