-- settings.lua

local vim = vim
local g = vim.g
local o = vim.o

-- disable netrw at the very start of your init.lua (strongly advised by nvim-tree)
g.loaded_netrw = 1
g.loaded_netrwPlugin = 1

-- set the mapleader
-- api.nvim_set_keymap('n', '<SPACE>', '<Nop>', { noremap = true, silent = true })
g.mapleader = " "
g.maplocalleader = " "

-- global options
o.encoding = "utf-8" -- file encoding
o.fileencoding = "utf-8" -- file encoding
o.fileencodings = "utf-8" -- file encoding
o.mouse = "a" -- use the mouse
o.clipboard = "unnamedplus" -- use the system clipboard
o.linebreak = true -- break lines at window limit
o.tabstop = 4 -- number of spaces for <tab>
o.shiftwidth = 4 -- number of spaces for indentation
o.softtabstop = 4 -- whitespace to be added
o.expandtab = true -- expand tab to spaces
o.smarttab = true -- tab in front of line inserts spaces acording to shiftwidth
o.autoindent = true -- copy indent from previous line
o.smartindent = true -- smart indenting for new line
o.termguicolors = true -- enable 24bit colors
o.textwidth = 92
o.colorcolumn = "93" -- add indicator for 96
o.cursorline = true -- highlight current line
-- o.hlsearch = false          -- highlight search results
o.smartcase = true -- try to be smart about cases in searches
o.autoread = true -- automatically read files after changes outside vim
o.backup = false -- no backup before overwriting a file
o.writebackup = false -- no backup before writing a file
o.signcolumn = "yes" -- always show signcolumn
o.scrolloff = 10 -- show lines above and below
o.number = true -- show line number
o.relativenumber = true -- show relative line number
o.hidden = true -- hide buffers when abandoned
o.updatetime = 500 -- set lower updatetime
o.lazyredraw = true -- only redraw when needed
o.syntax = "OFF"
o.undofile = true -- save history
o.completeopt = "menuone,noselect"
-- o.guifont = "JuliaNerd:h11"
o.guifont = "JuliaNerd:h11"
-- o.guicursor = "a:blinkwait3000-blinkon1000-blinkoff1000"

vim.diagnostic.config({
    update_in_insert = true,
    underline = { severity = vim.diagnostic.severity.ERROR },
    virtual_text = { severity = vim.diagnostic.severity.ERROR },
    float = {
        border = "rounded",
        source = "always",
    },
})

g.neovide_remember_window_size = false
if vim.loop.os_uname().sysname == "Windows_NT" then
    o.shell = "pwsh"
    o.shellcmdflag = "-NoLogo -NoProfile -ExecutionPolicy RemoteSigned -Command "
        .. "[Console]::InputEncoding=[Console]::OutputEncoding=[System.Text.Encoding]::UTF8;"
    o.shellredir = " -RedirectStandardOutput %s -NoNewWindow -Wait"
    o.shellpipe = " 2>&1 | Out-File -Encoding UTF8 %s; exit $LastExitCode"
    o.shellquote = '"'
    o.shellxquote = ""
end
