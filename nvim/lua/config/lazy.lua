-- bootstrap lazy
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
    vim.fn.system({
        "git",
        "clone",
        "--filter=blob:none",
        "https://github.com/folke/lazy.nvim.git",
        "--branch=stable",
        lazypath,
    })
end
vim.opt.rtp:prepend(lazypath)

-- lazy options
local opts = {
    defaults = { lazy = true },
    dev = { path = "~/Projects" },
    ui = { border = "rounded" },
}

-- setup lazy
require("lazy").setup("plugins", opts)

vim.keymap.set("n", "<leader>pl", "<cmd>Lazy<cr>", { desc = "[p]lugins [l]azy" })
vim.keymap.set("n", "<leader>ps", "<cmd>Lazy sync<cr>", { desc = "[p]lugins [s]ync" })
