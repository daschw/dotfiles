return {
    {
        "neovim/nvim-lspconfig",
        event = "BufReadPre",
        dependencies = {
            "hrsh7th/cmp-nvim-lsp",
            { "williamboman/mason.nvim", opts = { ui = { border = "rounded" } } },
            "williamboman/mason-lspconfig.nvim",
            "j-hui/fidget.nvim",
            "folke/neodev.nvim",
            "lukas-reineke/lsp-format.nvim",
            "nvim-telescope/telescope.nvim",
            {
                "lewis6991/hover.nvim",
                opts = {
                    init = function()
                        -- Require providers
                        require("hover.providers.lsp")
                        -- require('hover.providers.gh')
                        -- require('hover.providers.gh_user')
                        -- require('hover.providers.jira')
                        -- require('hover.providers.man')
                        -- require('hover.providers.dictionary')
                    end,
                    preview_opts = {
                        border = "rounded",
                    },
                    -- Whether the contents of a currently open hover window should be moved
                    -- to a :h preview-window when pressing the hover keymap.
                    preview_window = false,
                    title = true,
                },
                config = function(_, opts)
                    require("hover").setup(opts)
                end,
            },
            {
                "jose-elias-alvarez/null-ls.nvim",
                opts = {
                    sources = {
                        require("null-ls").builtins.formatting.stylua,
                    },
                },
            },
        },
        config = function()
            require("lsp-format").setup()
            local on_attach = function(client, bufnr)
                -- for LSP related items. It sets the mode, buffer and description for us
                -- each time.
                local nmap = function(keys, func, desc)
                    if desc then
                        desc = "LSP: " .. desc
                    end

                    vim.keymap.set("n", keys, func, { buffer = bufnr, desc = desc })
                end

                nmap("<leader>rn", vim.lsp.buf.rename, "[r]e[n]ame")
                nmap("<leader>ca", vim.lsp.buf.code_action, "[c]ode [a]ction")

                nmap("gd", vim.lsp.buf.definition, "[g]oto [d]efinition")
                nmap(
                    "gr",
                    require("telescope.builtin").lsp_references,
                    "[g]oto [r]eferences"
                )
                nmap("gI", vim.lsp.buf.implementation, "[g]oto [I]mplementation")
                nmap("<leader>D", vim.lsp.buf.type_definition, "Type [D]efinition")
                nmap(
                    "<leader>ds",
                    require("telescope.builtin").lsp_document_symbols,
                    "[d]ocument [s]ymbols"
                )
                nmap(
                    "<leader>ws",
                    require("telescope.builtin").lsp_dynamic_workspace_symbols,
                    "[w]orkspace [s]ymbols"
                )

                nmap("K", require("hover").hover, "hover documentation")
                -- nmap("K", vim.lsp.buf.hover, "hover documentation")
                nmap("<C-k>", vim.lsp.buf.signature_help, "signature documentation")

                nmap("gD", vim.lsp.buf.declaration, "[g]oto [D]eclaration")
                nmap(
                    "<leader>wa",
                    vim.lsp.buf.add_workspace_folder,
                    "[w]orkspace [a]dd folder"
                )
                nmap(
                    "<leader>wr",
                    vim.lsp.buf.remove_workspace_folder,
                    "[w]orkspace [r]emove folder"
                )
                nmap("<leader>wl", function()
                    print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
                end, "[w]orkspace [l]ist folders")

                require("lsp-format").on_attach(client)
            end

            local servers = {
                bashls = {},
                hls = {},
                julials = {},
                rust_analyzer = {},
                lua_ls = {},
                texlab = {},
                tsserver = {},
                nil_ls = {},
            }

            -- setup neovim lua configuration
            require("neodev").setup()

            -- nvim-cmp supports additional completion capabilities, so broadcast that to
            -- servers
            local capabilities = vim.lsp.protocol.make_client_capabilities()
            capabilities = require("cmp_nvim_lsp").default_capabilities(capabilities)

            -- setup mason so it can manage external tooling
            require("mason").setup()

            -- ensure the servers above are installed
            local mason_lspconfig = require("mason-lspconfig")

            mason_lspconfig.setup({
                ensure_installed = vim.tbl_keys(servers),
            })

            mason_lspconfig.setup_handlers({
                function(server_name)
                    require("lspconfig")[server_name].setup({
                        capabilities = capabilities,
                        on_attach = on_attach,
                        settings = servers[server_name],
                    })
                end,
            })

            -- turn on lsp status information
            require("fidget").setup({
                text = { spinner = "dots" },
                window = { border = "rounded" },
            })
        end,
    },
}
