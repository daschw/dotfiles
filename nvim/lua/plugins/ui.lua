local notes_dir
if vim.loop.os_uname().sysname == "Windows_NT" then
    notes_dir = "$HOME/Cloud/dani/notes"
else
    notes_dir = "$HOME/cloud/dani/notes"
end

return {
    -- colorscheme
    {
        "daschw/leaf.nvim",
        lazy = false,
        priority = 1000,
        dev = true,
        config = function()
            -- auto theme switching
            local fn = vim.fn.expand("~/dotfiles/nvim/lua/config/theme.lua")
            local function reload()
                vim.cmd("source " .. fn)
                require("leaf").setup({ contrast = CONTRAST })
                vim.api.nvim_command("colorscheme leaf")
            end

            local w = vim.loop.new_fs_event()
            local on_change
            local function watch_file(fname)
                w:start(fname, {}, vim.schedule_wrap(on_change))
            end

            function on_change()
                reload()
                w:stop()
                watch_file(fn)
            end

            -- reload vim config when background changes
            watch_file(fn)
            reload()
        end,
    },

    -- better ui
    {
        "stevearc/dressing.nvim",
        init = function()
            ---@diagnostic disable-next-line: duplicate-set-field
            vim.ui.select = function(...)
                require("lazy").load({ plugins = { "dressing.nvim" } })
                return vim.ui.select(...)
            end
            ---@diagnostic disable-next-line: duplicate-set-field
            vim.ui.input = function(...)
                require("lazy").load({ plugins = { "dressing.nvim" } })
                return vim.ui.input(...)
            end
        end,
    },

    -- statusline
    {
        "hoob3rt/lualine.nvim",
        dependencies = "nvim-tree/nvim-web-devicons",
        opts = { options = { globalstatus = true } },
    },

    -- tabline
    {
        "kdheepak/tabline.nvim",
        event = "BufEnter",
        dependencies = {
            "hoob3rt/lualine.nvim",
            "nvim-tree/nvim-web-devicons",
        },
        keys = {
            {
                "<A-B>",
                "<cmd>TablineBufferPrevious<cr>",
                desc = "Switch to previous buffer",
            },
            { "<A-b>", "<cmd>TablineBufferNext<cr>", desc = "Switch to next buffer" },
            { "<leader>bp", "<cmd>TablineBufferPrevious<cr>", desc = "[b]uffer [p]rev" },
            {
                "gB",
                "<cmd>TablineBufferPrevious<cr>",
                desc = "[g]oto previous [B]uffer",
            },
            { "<leader>bn", "<cmd>TablineBufferNext<cr>", desc = "[b]uffer [n]ext" },
            { "gb", "<cmd>TablineBufferNext<cr>", desc = "[g]oto next [b]uffer" },
            {
                "<leader>ba",
                "<cmd>TablineToggleShowAllBuffers<cr>",
                desc = "[b]uffer toggle [a]ll",
            },
        },
        opts = {
            enable = true,
            options = {
                -- set to nil by default, and it uses vim.o.columns * 2/3
                max_bufferline_percent = 86,
                show_tabs_always = true,
                show_devicons = true, -- this shows devicons in buffer section
                show_bufnr = false, -- this appends [bufnr] to buffer section,
                show_filename_only = false, -- shows base filename only instead of relative path in filename
                modified_icon = "+ ", -- change the default modified icon
                modified_italic = true, -- set to true by default; this determines whether the filename turns italic if modified
                show_tabs_only = false, -- this shows only tabs instead of tabs + buffers
            },
        },
    },

    -- indent guides
    {
        "lukas-reineke/indent-blankline.nvim",
        event = "BufReadPre",
        opts = {
            char = "┊",
            filetype_exclude = { "help", "alpha", "lazy" },
            show_end_of_line = true,
            space_char_blankline = " ",
        },
        config = function(_, opts)
            vim.opt.list = true
            vim.opt.listchars:append("eol:↴")
            require("indent_blankline").setup(opts)
        end,
    },

    -- color for csv
    {
        "mechatroner/rainbow_csv",
        ft = "csv",
    },

    -- preview colors
    {
        "NvChad/nvim-colorizer.lua",
        event = "BufReadPost",
        opts = {
            user_default_options = {
                names = false,
                mode = "virtualtext",
            },
        },
        config = function(_, opts)
            require("colorizer").setup(opts)
        end,
    },

    -- underline word under cursor
    { "yamatsum/nvim-cursorline" },

    -- animations
    {
        "declancm/cinnamon.nvim",
        event = "BufReadPost",
        opts = {
            extra_keymaps = true,
            extended_keymaps = true,
            max_length = -1,
            scroll_limit = 150,
        },
        config = function(_, opts)
            require("cinnamon").setup(opts)
        end,
    },

    -- dashboard
    {
        "glepnir/dashboard-nvim",
        event = "VimEnter",
        opts = {
            -- config
            theme = "hyper",
            config = {
                header = {
                    [[    ,-.       _,---._ __  / \  ]],
                    [[   /  )    .-'       `./ /   \ ]],
                    [[  (  (   ,'            `/    /|]],
                    [[   \  `-"             \'\   / |]],
                    [[    `.              ,  \ \ /  |]],
                    [[     /`.          ,'-`----Y   |]],
                    [[    (            ;        |   ']],
                    [[    |  ,-.    ,-'         |  / ]],
                    [[    |  | (   |            | /  ]],
                    [[    )  |  \  `.___________|/   ]],
                    [[    `--'   `--'                ]],
                    [[]],
                    [[]],
                    -- [[]],
                },
                footer = {
                    -- [[]],
                    [[]],
                    [[]],
                    [[        |\      _,,,---,,_     ]],
                    [[  ZZZzz /,`.-'`'    -.  ;-;;,_ ]],
                    [[       |,4-  ) )-,_. ,\ (  `'-']],
                    [[      '---''(_/--'  `-'\_)     ]],
                },
                shortcut = {
                    {
                        desc = "ﮮ Update",
                        group = "Type",
                        action = "Lazy update",
                        key = "u",
                    },
                    {
                        desc = " Files",
                        group = "Directory",
                        action = "Telescope find_files",
                        key = "f",
                    },
                    {
                        desc = " Recent",
                        group = "Constant",
                        action = "Telescope smart_open",
                        key = "r",
                    },
                    {
                        desc = " Projects",
                        group = "SmoothCursorPurple",
                        action = "Telescope projects",
                        key = "p",
                    },
                    -- {
                    --     desc = " dotfiles",
                    --     group = "PreProc",
                    --     action = "cd $HOME/dotfiles/ | Telescope find_files",
                    --     key = "d",
                    -- },
                    -- {
                    --     desc = " Neorg",
                    --     group = "PreProc",
                    --     -- group = "Special",
                    --     action = "cd " .. notes_dir .. " | Telescope find_files",
                    --     key = "o",
                    -- },
                    {
                        desc = " Quit",
                        group = "Special",
                        -- group = "Comment",
                        action = "qa",
                        key = "q",
                    },
                },
            },
        },
        dependencies = { "nvim-tree/nvim-web-devicons" },
    },
}
