return {
    -- which-key
    {
        "folke/which-key.nvim",
        event = "VeryLazy",
        opts = {
            plugins = { spelling = true },
        },
        config = function(_, opts)
            vim.g.timoutlen = 0
            local wk = require("which-key")
            wk.setup(opts)
            wk.register({
                mode = { "n", "v" },
                ["g"] = { name = "+goto" },
                ["]"] = { name = "+next" },
                ["["] = { name = "+prev" },
                ["<leader>b"] = { name = "+buffer" },
                ["<leader>g"] = { name = "+git" },
                -- ["<leader>o"] = { name = "+neorg" },
                ["<leader>p"] = { name = "+plugins" },
                ["<leader>r"] = { name = "+repl" },
                ["<leader>s"] = { name = "+search" },
                ["<leader>t"] = { name = "+tree" },
            })
        end,
    },

    -- tree
    {
        "nvim-tree/nvim-tree.lua",
        dependencies = { "nvim-tree/nvim-web-devicons" },
        cmd = "NvimTreeToggle",
        opts = {
            sync_root_with_cwd = true,
            respect_buf_cwd = true,
            update_focused_file = {
                enable = true,
                update_root = true,
            },
            view = { adaptive_size = true },
            renderer = { indent_markers = { enable = true } },
            git = { ignore = false },
        },
        keys = {
            { "<leader>tt", "<cmd>NvimTreeFindFileToggle<cr>", desc = "[t]ree [t]oggle" },
            { "<leader>tr", "<cmd>NvimTreeRefresh<cr>",        desc = "[t]ree [r]efresh" },
        },
    },

    -- telescope
    {
        "nvim-telescope/telescope.nvim",
        dependencies = {
            { "nvim-telescope/telescope-fzf-native.nvim", build = "make" },
            "nvim-lua/popup.nvim",
            "nvim-lua/plenary.nvim",
            "folke/todo-comments.nvim",
            {
                "ahmedkhalf/project.nvim",
                event = "VeryLazy",
                opts = {
                    patterns = {
                        ".git",
                        "_darcs",
                        ".hg",
                        ".bzr",
                        ".svn",
                        "Makefile",
                        "package.json",
                        ">Projects",
                    },
                    show_hidden = true,
                },
                config = function(_, opts)
                    require("project_nvim").setup(opts)
                end,
            },
            "nvim-telescope/telescope-symbols.nvim",
            "nvim-telescope/telescope-media-files.nvim",
            {
                "danielfalk/smart-open.nvim",
                dependencies = "kkharji/sqlite.lua",
            },
        },
        cmd = "Telescope",
        keys = {
            {
                "<leader>/",
                "<cmd>Telescope current_buffer_fuzzy_find<cr>",
                "[/] fuzzy search in current buffer",
            },
            {
                "<leader>?",
                "<cmd>Telescope oldfiles<cr>",
                "[?] find recently opened files",
            },
            { "<leader>sb", "<cmd>Telescope buffers<cr>",     "[s]earch [b]uffers" },
            { "<leader>sd", "<cmd>Telescope diagnostics<cr>", "[s]earch [d]iagnostics" },
            { "<leader>sf", "<cmd>Telescope find_files<cr>",  "[s]earch [f]iles" },
            { "<leader>sg", "<cmd>Telescope live_grep<cr>",   "[s]earch by [g]rep" },
            { "<leader>sh", "<cmd>Telescope help_tags<cr>",   "[s]earch [h]elp" },
            { "<leader>sm", "<cmd>Telescope media_files<cr>", "[s]earch [m]edia files" },
            { "<leader>sp", "<cmd>Telescope projects<cr>",    "[s]earch [p]rojects" },
            { "<leader>ss", "<cmd>Telescope smart_open<cr>",  "[s]earch [s]mart" },
            { "<leader>sw", "<cmd>Telescope grep_string<cr>", "[s]earch current [w]ord" },
        },
        opts = {
            defaults = {
                layout_strategy = "flex",
                -- layout_config = { height = 0.95 },
            },
        },
        config = function(_, opts)
            local telescope = require("telescope")
            telescope.setup(opts)
            telescope.load_extension("fzf")
            telescope.load_extension("projects")
            telescope.load_extension("smart_open")
            telescope.load_extension("media_files")
        end,
    },

    -- gitsigns
    {
        "lewis6991/gitsigns.nvim",
        event = "BufReadPre",
        dependencies = {
            "nvim-lua/plenary.nvim",
        },
        opts = {
            signs = {
                add = { text = "+" },
                change = { text = "~" },
                delete = { text = "_" },
                topdelete = { text = "‾" },
                changedelete = { text = "~" },
            },
        },
    },

    -- search and replace
    {
        "windwp/nvim-spectre",
        dependencies = {
            "nvim-lua/plenary.nvim",
        },
        keys = {
            {
                "<leader>sr",
                function()
                    require("spectre").open()
                end,
                desc = "[s]earch and [r]eplace",
            },
        },
    },

    -- neogit
    {
        "TimUntersberger/neogit",
        dependencies = {
            "nvim-lua/plenary.nvim",
            "sindrets/diffview.nvim",
        },
        keys = {
            {
                "<leader>gg",
                "<cmd>lua require('neogit').open()<cr>",
                desc = "open neo[g]it",
            },
        },
        opts = { integrations = { diffview = true } },
    },

    -- remove buffers
    {
        "kazhala/close-buffers.nvim",
        keys = {
            {
                "<leader>bd",
                function()
                    require("close_buffers").delete({ type = "this" })
                end,
                desc = "[b]uffer [d]elete",
            },
            {
                "<leader>bD",
                function()
                    require("close_buffers").delete({ type = "this", force = true })
                end,
                desc = "[b]uffer [D]elete (force)",
            },
            {
                "<leader>bo",
                function()
                    require("close_buffers").delete({ type = "other" })
                end,
                desc = "[b]uffer delete [o]thers",
            },
            {
                "<leader>bO",
                function()
                    require("close_buffers").delete({ type = "other", force = true })
                end,
                desc = "[b]uffer delete [O]thers (force)",
            },
        },
    },

    -- save protected files
    {
        "lambdalisue/suda.vim",
        lazy = false,
    },

    -- toggleterm
    {
        "akinsho/toggleterm.nvim",
        dependencies = { "nvim-treesitter/nvim-treesitter" },
        lazy = false,
        -- cmd = "ToggleTerm",
        keys = function()
            local function replace_termcodes(str)
                return vim.api.nvim_replace_termcodes(str, true, true, true)
            end

            return {
                -- TODO: <esc>
                {
                    "<esc>",
                    replace_termcodes("<C-\\><C-N>"),
                    desc = "escape terminal",
                    mode = "t",
                },
                { "<leader>rr", "<cmd>ToggleTerm<cr>", desc = "Toggle [r]epl" },
                {
                    "<leader>rh",
                    "<cmd>ToggleTerm direction=horizontal<cr>",
                    desc = "Toggle [r]epl [h]orizontally",
                },
                {
                    "<leader>rv",
                    "<cmd>ToggleTerm direction=vertical<cr>",
                    desc = "Toggle [r]epl [v]ertically",
                },
                {
                    "<leader>rf",
                    "<cmd>ToggleTerm direction=float<cr>",
                    desc = "Toggle [r]epl [f]loating",
                },
            }
        end,
        opts = {
            -- shade_terminals = false,
            shade_terminals = true,
            shading_factor = 0.23,
            open_mapping = [[<c-t>]],
            start_in_insert = false,
            size = function(term)
                if term.direction == "horizontal" then
                    return 15
                elseif term.direction == "vertical" then
                    return vim.o.columns * 0.33
                end
            end,
            auto_scroll = true,
            highlights = {
                -- Normal = { link = "CursorLine" },
                SignColumn = { guibg = "NONE" },
            },
            -- float_opts = {
            --     width = 80,
            --     height = 20,
            -- },
        },
        config = function(_, opts)
            require("toggleterm").setup(opts)
            local Terminal = require("toggleterm.terminal").Terminal
            local treesitter = require("vim.treesitter")
            local ts_utils = require("nvim-treesitter.ts_utils")

            -- to avoid autoindent in the julia REPL put this in your
            -- ~/.julia/config/startup.jl
            --[[
            import REPL
            REPL.GlobalOptions.auto_indent = false
            REPL.LineEdit.options(::REPL.LineEdit.PromptState) = REPL.GlobalOptions
            --]]
            local repls = {
                julia = Terminal:new({ cmd = "julia --project=." }),
                python = Terminal:new({ cmd = "ipython --no-autoindent" }),
                lua = Terminal:new({ cmd = "lua" }),
                nu = Terminal:new({ cmd = "nu" }),
                shell = Terminal:new(),
            }

            local function treesitter_lang(buf)
                local parser = treesitter.get_parser(buf)
                return parser:lang()
            end

            local function treesitter_repl(buf)
                local lang = treesitter_lang(buf)
                return repls[lang]
            end

            local function current_root_node(win)
                local node = ts_utils.get_node_at_cursor(win)
                if node == nil then
                    return
                end
                local parent = node:parent()
                if parent == nil then
                    return node
                end
                local root = parent:parent()
                while root ~= nil do
                    node = parent
                    parent = root
                    root = root:parent()
                end
                return node
            end

            local function repl_cmd(node, buf)
                local lines = treesitter.get_node_text(node, buf)
                local cmd
                if type(lines) == "string" then
                    cmd = lines
                elseif type(lines) == "table" then
                    -- cmd = table.concat(trim(lines), "\n")
                    cmd = table.concat(lines, "\n")
                end
                return cmd
            end

            local function next_executable_node(node)
                local next = ts_utils.get_next_node(node, false, false)
                while next and string.find(next:type(), "comment") do
                    node = next
                    next = ts_utils.get_next_node(node, false, false)
                end
                return next
            end

            _G.run_in_repl = function()
                -- get current window and buffer
                local win = vim.api.nvim_get_current_win()
                local buf = vim.api.nvim_win_get_buf(win)
                -- find a matching repl
                local repl = treesitter_repl(buf) or repls["shell"]
                -- get the current executable root node
                local root = current_root_node(win)
                -- open the repl or send a command
                if not repl:is_open() then
                    repl:open()
                else
                    -- get the command to send to repl
                    local cmd = repl_cmd(root, buf)
                    repl:send(cmd, false)
                    if repl == repls["python"] then
                        repl:send("\n")
                    end
                    -- go to next executable node
                    root = next_executable_node(root)
                end
                vim.api.nvim_set_current_win(win)
                ts_utils.goto_node(root, false, false)
            end

            _G.build_docs = function()
                -- get current window, buffer and filename
                local win = vim.api.nvim_get_current_win()
                local buf = vim.api.nvim_win_get_buf(win)
                -- get the matching language
                local lang = treesitter_lang(buf)
                if lang == "julia" then
                end
            end

            vim.keymap.set(
                "n",
                "<leader><leader>",
                "<cmd>lua run_in_repl()<CR>",
                { desc = "[r]un in [r]epl" }
            )
            vim.keymap.set(
                "n",
                "<C-cr>",
                "<cmd>lua run_in_repl()<CR>",
                { desc = "[r]un in [r]epl" }
            )
        end,
    },
}
