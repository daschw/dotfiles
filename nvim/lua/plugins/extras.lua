return {
    -- tex
    {
        "lervag/vimtex",
        ft = { "tex", "bib" },
        config = function()
            vim.g.vimtex_compiler_latexmk = {
                options = {
                    "-verbose",
                    "-file-line-error",
                    "-synctex=1",
                    "-interaction=nonstopmode",
                    "-shell-escape",
                },
            }
            vim.g.vimtex_view_general_viewer = "okular"
            vim.g.vimtex_view_general_options = "--unique file:@pdf#src:@line@tex"
        end,
    },

    -- neorg
    -- {
    --     "nvim-neorg/neorg",
    --     -- version = "0.0.14",
    --     build = ":Neorg sync-parsers",
    --     ft = "norg",
    --     keys = {
    --         {
    --             "<leader>on",
    --             "<cmd>Neorg workspace notes<cr>",
    --             desc = "ne[o]rg open [n]otes workspace",
    --         },
    --         {
    --             "<leader>oh",
    --             "<cmd>Neorg workspace home<cr>",
    --             desc = "ne[o]rg open [h]ome workspace",
    --         },
    --         {
    --             "<leader>ow",
    --             "<cmd>Neorg workspace work<cr>",
    --             desc = "ne[o]rg open [w]ork workspace",
    --         },
    --         -- { "<leader>oc", "<cmd>Neorg gtd capture<cr>", desc = "ne[o]rg [c]apture tasks" },
    --         -- { "<leader>ov", "<cmd>Neorg gtd views<cr>", desc = "ne[o]rg [v]iew tasks" },
    --     },
    --     opts = {
    --         load = {
    --             ["core.defaults"] = {},
    --             ["core.norg.completion"] = { config = { engine = "nvim-cmp" } },
    --             ["core.integrations.nvim-cmp"] = {},
    --             ["core.norg.dirman"] = {
    --                 config = {
    --                     workspaces = {
    --                         work = "~/cloud/dani/notes/work",
    --                         home = "~/cloud/dani/notes/home",
    --                         notes = "~/cloud/dani/notes",
    --                     },
    --                 },
    --             },
    --             ["core.norg.concealer"] = {
    --                 config = { folds = false },
    --             },
    --             -- ["core.gtd.base"] = {
    --             --     config = {
    --             --         workspace = "notes",
    --             --     },
    --             -- },
    --         },
    --     },
    -- },

    -- ron syntax highlighting
    {
        "ron-rs/ron.vim",
        ft = "ron",
    },

    -- nu shell
    {
        "LhKipp/nvim-nu",
        dependencies = {
            "nvim-treesitter/nvim-treesitter",
            "jose-elias-alvarez/null-ls.nvim",
        },
        lazy = false,
        ft = "nu",
        build = ":TSInstall nu",
        config = function()
            require("nu").setup({})
        end,
    },

    -- yuck filetype support for eww widgets
    {
        "elkowar/yuck.vim",
        ft = "yuck",
    },

    -- filetype support for .rasi (rofi config)
    {
        "Fymyte/rasi.vim",
        ft = "rasi",
    },

    -- filetype support for zellij config
    {
        "imsnif/kdl.vim",
        ft = "kdl",
    },
}
