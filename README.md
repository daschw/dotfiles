# dotfiles

backup of my personal configuration for nvim, kitty, fish, bash and others.

## install

1. clone the repo into a local directory, e.g. `~/dotfiles`
    ```fish
    git clone https://codeberg.org/daschw/dotfiles ~/dotfiles
    ```
    or
    ```fish
    git clone git@codeberg.org:daschw/dotfiles ~/dotfiles
    ```
2. navigate to the cloned directory
    ```fish
    cd ~/dotfiles
    ```
3. run the `bootstrap.sh` script
    ```fish
    ./bootstrap.sh
    ```
    this should install all required packages from the official arch repos and create the
    required symlinks to your `~/dotfiles` repo
4. to install the packages from the AUR first install
    [paru](https://github.com/Morganamilo/paru)
    ```fish
    sudo pacman -S --needed base-devel
    git clone https://aur.archlinux.org/paru.git
    cd paru
    makepkg -si
    ```
    then navigate to your dotfiles repo
    ```fish
    cd ~/dotfiles
    ```
    and run
    ```fish
    paru -S --needed - < /packages/foreignpkglist.txt
    ```
