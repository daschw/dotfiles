; ------------------------------------------------------------------------------------------
; variables
; ------------------------------------------------------------------------------------------

(defpoll wifi_info :interval "1s" "scripts/wifi --info")
(defvar wifi_list "scripts/wifi --list")

; ------------------------------------------------------------------------------------------
; widget
; ------------------------------------------------------------------------------------------

(defwidget wifi []
  (button
    :class "bar_button"
    :onclick "scripts/wifi --popup"
    :onrightclick "scripts/wifi --toggle"
    :timeout "12s" ; need this to work
    :tooltip "connected to ${wifi_info.name}"
    (button_box :state "${wifi_info.status}" "${wifi_info.icon}")))

; ------------------------------------------------------------------------------------------
; popup
; ------------------------------------------------------------------------------------------

(defwidget wifi_connect_button [class name protected bars]
  (button
    :class "bar_button"
    :onclick "${eww} update wifi_list='$(~/.config/hypr/eww/bar/scripts/wifi --list)' && \
      scripts/wifi --connect '${name}' ${protected}"
    (box
      :class class
      :orientation "h"
      :space-evenly false
      :spacing 16
      :height 24
      :width 216
      (label :text {protected ? "" : ""})
      (label :text name :limit-width 11 :width 130 :vexpand true :xalign 0)
      (label :text bars))))

(defwidget wifi_list_box []
  (box
    :class "sidebar_box"
    (sidebar_box
      :state "wifi_list"
        (scroll
          :height { hypr.focused_monitor.height / hypr.focused_monitor.scale - 90 }
          :vexpand true
          (box
            :orientation "v"
            :space-evenly false
            :valign "end"
            (for item in wifi_list
              (wifi_connect_button
                :class {item.active == "*" ? "wifi_active" : "wifi"}
                :name "${item.ssid}"
                :protected "${strlength(item.security) > 0}"
                :bars "${item.bars}")))))))

(defwidget wifi_sidebar_widget []
    (sidebar_widget
      (sidebar_module (wifi_list_box))))

(defwindow wifi_sidebar
  :geometry (geometry :anchor "center right")
  :exclusive true
  (wifi_sidebar_widget))
