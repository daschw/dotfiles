#!/usr/bin/env bash

main() {
    if [[ "$1" == "--listen-metadata" ]]; then
        listen_metadata
    elif [[ "$1" == "--listen-position" ]]; then
        listen_position
    elif [[ "$1" == "--listen-status" ]]; then
        listen_status
    fi
}

listen_status() {
    playerctl --follow status | while read -r status; do
        if [[ $status != "Playing" ]]; then
            icon=""
        else
            icon=""
        fi
        echo "{\"status\":\"$status\",\"icon\":\"$icon\"}"
    done
}

listen_position() {
    fmt="{\"position\":\"{{position}}\",\"time\":\"{{duration(position)}}\"}"
    playerctl --follow position --format "$fmt"
}

listen_metadata() {
    fmt="\"artist\":\"{{artist}}\",\"title\":\"{{title}}\",\"album\":\"{{album}}\""
    fmt="$fmt,\"arturl\":\"{{mpris:artUrl}}\",\"length\":\"{{mpris:length}}\""
    fmt="$fmt,\"duration\":\"{{duration(mpris:length)}}\""
    playerctl --follow metadata --format "{$fmt}" | while read -r info; do
        artist=$(echo -e $info | jq -r ".artist")
        title=$(echo -e $info | jq -r ".title")
        url=$(echo -e $info | jq -r ".arturl")
        cover=$(cover_art "$url")
        lyrics=$(lyrics_lines "$artist" "$title")
        echo $info | jq -cr ". + {\"cover\":\"$cover\",\"lyrics\":\"$lyrics\"}"
    done
}

cover_art() {
    mkdir -p ~/.cache/eww/music
    cover_file="$HOME/.cache/eww/music/$(echo "$1" | awk -F / '{print $5}').jpg"
    if [[ ! -f "$cover_file" ]]; then
        curl -s "$1" --output "$cover_file"
    fi
    echo "$cover_file"
}

lyrics_lines() {
    inner=$( \
        echo "$(lyrics -t "$1" "$2")" | \
        tail -n +4 | \
        sed "s/[][\"]/'\&'/g" | \
        sed -E "s/(.*)/\\\\\\\"\1\\\\\\\"/g" | \
        awk -v d="," '{s=(NR==1?s:s d)$0}END{print s}' \
    )
    echo -e "[$inner]"
}

main "$@"
