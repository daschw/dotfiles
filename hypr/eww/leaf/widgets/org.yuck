; ------------------------------------------------------------------------------------------
; variables
; ------------------------------------------------------------------------------------------

(defvar ORG_REVEAL false)
(defvar TASKWARRIOR_LIST "[]")
(defvar TASKWARRIOR_TIMES "")
(defpoll TIME
  :interval "1s"
  "date \
  '+{\"date\":\"%F\",\"year\":\"%Y\",\"day\":\"%d\",\"hour\":\"%I\",\"minute\":\"%M\"}'")

; ------------------------------------------------------------------------------------------
; bar button
; ------------------------------------------------------------------------------------------

(defwidget leaf-org []
  (leaf-bar-button
    :buttonclass ""
    :icon "${ TIME.hour }:${ TIME.minute }"
    :popup "org"
    :reveal "ORG_REVEAL"
    :revealer ORG_REVEAL
    :text "${ TIME.date }"
    :visible true))

; ------------------------------------------------------------------------------------------
; sidebar window
; ------------------------------------------------------------------------------------------

(defwindow leaf-org-sidebar
  :geometry (geometry
    :anchor "center right")
  :stacking "fg"
  :exclusive true
  (leaf-org-sidebar-layout))

; ------------------------------------------------------------------------------------------
; sidebar widgets
; ------------------------------------------------------------------------------------------

(defwidget leaf-org-sidebar-layout []
  (leaf-sidebar-layout
    (leaf-sidebar-module
      (leaf-sidebar-content
        (box
          :orientation "v"
          :space-evenly false
          :spacing 4
          (label
            :class "comment"
            :text "calendar")
          (calendar
            :class "calendar"
            :day "${ TIME.day }"
            :year "${ TIME.year }"))))
    (leaf-sidebar-module
      (leaf-sidebar-content
        (box
          :space-evenly false
          :orientation "v"
          :spacing 4
          (label
            :class "comment"
            :text "pending tasks")
          (scroll
            :hscroll false
            :height { SIDEBAR_HEIGHT - 250 }
              (box
                :orientation "v"
                :space-evenly false
                (for task in TASKWARRIOR_LIST
                  (box
                    :class "leaf-task-module ${ task.start == "null" ? "" : "active" }"
                    :orientation "v"
                    (box
                      :space-evenly false
                      :spacing 4
                      (label
                        :class "comment"
                        :width 32
                        :text "age:")
                      (label
                        :class "purple"
                        :width 32
                        :justify "right"
                        :xalign 1
                        :text { TASKWARRIOR_TIMES["${task.id}"].age })
                      (box
                        :width 46)
                      (label
                        :class "comment"
                        :width 32
                        :text "due:")
                      (label
                        :class {
                          task.urgency < 5 ? "green" :
                          task.urgency < 7 ? "yellow" :
                          task.urgency < 9 ? "orange" : "red" }
                        :width 32
                        :justify "right"
                        :xalign 1
                        :text { TASKWARRIOR_TIMES["${task.id}"].due }))
                    (label
                      :wrap true
                      :halign "start"
                      :justify "left"
                      :xalign 0
                      :hexpand true
                      :text "${ task.description }")
                    (box
                      :space-evenly false
                      :halign "start"
                      :spacing 4
                      (label
                        :class "comment"
                        :text "tags:")
                      (box
                        :spacing 4
                        (for tag in "${ task.tags }"
                          (label
                            :class "teal"
                            :text tag)))))))))))))
