#!/usr/bin/env bash

# ----------------------------------------------------------------------------------------------
# install required packages
# sudo pacman -S --needed - < "$(pwd)/packages/pkglist.txt"

# ----------------------------------------------------------------------------------------------
# creating necessary directories
mkdir -p $HOME/.config
mkdir -p $HOME/.config/autostart-scripts
mkdir -p $HOME/.config/plasma-workspace/shutdown
mkdir -p $HOME/.config/plasma-workspace/env
mkdir -p $HOME/Projects

echo "Symlinking dotfiles to $HOME/.config/"
progs=( \
    bottom \
    cava \
    fish \
    foot \
    gitui \
    himalaya \
    hypr \
    kanshi \
    kitty \
    nvim \
    nushell \
    picom \
    redshift \
    reflector \
    rofi \
    spotify-tui \
    spotifyd \
    wezterm \
)

for prog in "${progs[@]}"
do
    rm -rf "$HOME/.config/$prog"
    ln -s "$(pwd)/$prog" $HOME/.config
done

# ----------------------------------------------------------------------------------------------
# removing old files
rm -f $HOME/.bashrc
rm -f $HOME/.bash_aliases
rm -f $HOME/.gitconfig
rm -f $HOME/.config/autostart-scripts/ssh-add.sh
rm -f $HOME/.config/plasma-workspace/shutdown/ssh-agent-shutdown.sh
rm -f $HOME/.config/plasma-workspace/env/ssh-agent-startup.sh
rm -f $HOME/.config/starship.toml
rm -f $HOME/.davmail.properties
rm -f $HOME/.julia/config/startup.jl

# ----------------------------------------------------------------------------------------------
# symlinking config files
ln -s "$(pwd)/bashrc" $HOME/.bashrc
ln -s "$(pwd)/bash_aliases" $HOME/.bash_aliases
ln -s "$(pwd)/gitconfig" $HOME/.gitconfig
ln -s "$(pwd)/kde/ksshaskpass/ssh-agent-shutdown.sh" \
    $HOME/.config/plasma-workspace/shutdown/ssh-agent-shutdown.sh
ln -s "$(pwd)/kde/ksshaskpass/ssh-agent-startup.sh" \
    $HOME/.config/plasma-workspace/env/ssh-agent-startup.sh
ln -s "$(pwd)/starship.toml" $HOME/.config/starship.toml
ln -s "$(pwd)/davmail.properties" $HOME/.davmail.properties
ln -s "$(pwd)/julia/config/startup.jl" $HOME/.julia/config/startup.jl

# ----------------------------------------------------------------------------------------------
# symlinking into $HOME/.config/autostart-scripts/ does not work apparently. So we have to copy.
echo "Copying to $HOME/.config/autostart-scripts/"
cp "$(pwd)/kde/ksshaskpass/ssh-add.sh" $HOME/.config/autostart-scripts/ssh-add.sh

# ----------------------------------------------------------------------------------------------
# leaf nvim theme
if [[ ! -d $HOME/Projects/leaf.nvim/ ]]
then
    echo "Cloning leaf.nvim theme"
    git clone https://github.com/daschw/leaf.nvim $HOME/Projects/leaf.nvim
fi

# ---------------------------------------------------------------------------------------------- 
# theme config
if [[ ! -f "$(pwd)/theme.conf" ]]; then
    echo "dark" > "$(pwd)/theme.conf"
fi
if [[ ! -f "$(pwd)/nvim/lua/config/theme.lua" ]]; then
    echo "vim.o.background = \"dark\"" > "$(pwd)/nvim/lua/config/theme.lua"
fi
if [[ ! -f $HOME/.Xresources ]]; then
    ln -s $HOME/Projects/leaf.nvim/extras/Xresources/leaf-dark.Xresources $HOME/.Xresources
fi
if [[ ! -f "$(pwd)/fish/theme.fish" ]]; then
    ln -s $HOME/Projects/leaf.nvim/extras/fish/leaf-dark.fish "$(pwd)/fish/theme.fish"
fi
if [[ ! -f "$(pwd)/kitty/theme.conf" ]]; then
    ln -s $HOME/Projects/leaf.nvim/extras/kitty/leaf-dark.conf "$(pwd)/kitty/theme.conf"
fi
if [[ ! -f "$(pwd)/wezterm/theme.lua" ]]; then
    ln -s $HOME/Projects/leaf.nvim/extras/wezterm/leaf-dark.lua "$(pwd)/wezterm/theme.lua"
fi
if [[ ! -f $HOME/.julia/config/leaf.jl ]]; then
    ln -s $HOME/Projects/leaf.nvim/extras/julia/leaf.jl $HOME/.julia/config/leaf.jl
fi
if [[ ! -f "$(pwd)/hypr/current_theme" ]]; then
    echo "dark" > "$(pwd)/hypr/current_theme"
fi
if [[ ! -f "$(pwd)/hypr/mako/config" ]]; then
    ln -s "$(pwd)/hypr/mako/leaf-dark" "$(pwd)/hypr/mako/config"
fi
if [[ ! -f "$(pwd)/hypr/waybar/theme.css" ]]; then
    ln -s "$(pwd)/hypr/waybar/themes/leaf-dark.css" "$(pwd)/hypr/waybar/theme.css"
fi
if [[ ! -f "$(pwd)/hypr/eww/bar/_colors.scss" ]]; then
    ln -s "$(pwd)/hypr/eww/bar/colors/leaf-dark.scss" "$(pwd)/hypr/eww/bar/_colors.scss"
fi
if [[ ! -f "$(pwd)/hypr/theme.conf" ]]; then
    ln -s "$(pwd)/hypr/themes/leaf-dark.conf" "$(pwd)/hypr/theme.conf"
fi
