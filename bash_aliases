#!/usr/bin/env bash

alias vi='nvim'
alias please='sudo'
alias ls='exa --long --header --git --icons --tree --level=1 --sort=type'
alias cd='z'
alias :q='exit'
alias :qa='exit'
alias fx='felix'
alias g='gitui'
alias sp='spotify_player'
alias tw='taskwarrior-tui'

# alias ghc="$(ghcup whereis ghc)"
# alias haskell-language-server-wrapper="$(ghcup whereis hls)"
