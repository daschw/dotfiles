def bootstrap [] {
    for program in [
        "bottom",
        "cava",
        "fish",
        "gitui",
        "kitty",
        "nvim",
        "nushell",
        "spotify-player",
        "vivid",
        "ezterm",
        "zellij",
    ] { ln -s $"~/dotfiles/($program)" ~/.config/ }
}
