def cat_fortune [i: int] {
    let files = (ls -f ~/dotfiles/fish/cats/ | get name)
    let ascii_cat = (open --raw ($files | get ($i mod ($files | length))))
    let colors = ["red", "green", "yellow", "purple", "blue", "cyan"]
    let color = ($colors | get ((random integer) mod ($colors | length)))
    $"(ansi $color)\n($ascii_cat)\n\n(fortune)"
}

cat_fortune 5
