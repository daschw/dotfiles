import REPL
REPL.GlobalOptions.auto_indent = false
REPL.LineEdit.options(s::REPL.LineEdit.PromptState) = REPL.GlobalOptions

try
    using Revise
catch e
    @warn "Error initializing Revise" exception = (e, catch_backtrace())
end

try
    import OhMyREPL
    include(joinpath(@__DIR__, "leaf.jl")) # add leaf colorschemes to OhMyREPL
    OhMyREPL.enable_autocomplete_brackets(false)
    OhMyREPL.colorscheme!(string("leaf ", get(ENV, "THEME", "dark")))
    OhMyREPL.colorscheme!(
        string(
            "leaf ",
            ifelse(
                let fn = joinpath(homedir(), "dotfiles", "theme.conf")
                    isfile(fn) && occursin("dark", read(fn, String))
                end,
                "dark",
                "light",
            ),
        )
    )
catch e
    @warn(
        "Error initializing OhMyREPL. Make sure to also `Pkg.add(\"Crayons\")",
        exception = (e, catch_backtrace()),
    )
end

# try
#     using AbbreviatedStackTraces
# catch e
#     @warn "Error initializing AbbreviatedStackTraces" exception = (e, catch_backtrace())
# end

@static if Sys.islinux()
    # set ssh key paths
    if !haskey(ENV, "SSH_KEY_PATH")
        ENV["SSH_KEY_PATH"] = joinpath(homedir(), ".ssh", "id_rsa")
    end
    if !haskey(ENV, "SSH_PUB_KEY_PATH")
        ENV["SSH_PUB_KEY_PATH"] = joinpath(homedir(), ".ssh", "id_rsa.pub")
    end
    # Fix ssh-agent in vscode
    if !haskey(ENV, "SSH_AUTH_SOCK")
        ENV["SSH_AUTH_SOCK"] = joinpath(ENV["XDG_RUNTIME_DIR"], "ssh-agent.socket")
    end

    # set GUROBI_HOME for vscode
    if !haskey(ENV, "GUROBI_HOME")
        ENV["GUROBI_HOME"] = "/opt/gurobi950/linux64/"
    end

    # Paths for data packages
    const DATA_PATH = joinpath(homedir(), "cloud", "work", "Data")
else
    const DATA_PATH = joinpath(homedir(), "Cloud", "Work", "Data")
end

ENV["DATA_PATH"] = DATA_PATH
ENV["ENTSOE_DATA_PATH"] = joinpath(DATA_PATH, "ENTSOE")
ENV["RENEWABLES_NINJA_DATA_PATH"] = joinpath(DATA_PATH, "RenewablesNinja")
ENV["NUTS_DATA_PATH"] = joinpath(DATA_PATH, "NUTS")
ENV["HOTMAPS_DATA_PATH"] = joinpath(DATA_PATH, "Hotmaps")
ENV["AUSTRIAN_HEATMAP_DATA_PATH"] = joinpath(DATA_PATH, "AustrianHeatmap")
ENV["MAP_AUSTRIA_DATA_PATH"] = joinpath(DATA_PATH, "MapAustria")
ENV["WHEN2HEAT_DATA_PATH"] = joinpath(DATA_PATH, "When2Heat")
ENV["APCS_DATA_PATH"] = joinpath(DATA_PATH, "APCS")

# define a macro to register EEG packages
macro register(pkg)
    quote
        using Pkg
        using $pkg
        using LocalRegistry
        LocalRegistry.register(
            $pkg;
            registry=joinpath(homedir(), ".julia", "registries", "EnSysRegistry"),
            repo=string("git@gitlab.com:team-ensys/", $pkg, ".jl.git")
        )
    end
end

# using PkgTemplates
# t = Template(
#    julia_version = v"1.4",
#    dev = false,
#    plugins = [
#        AppVeyor(),
#        Codecov(),
#        Coveralls(),
#        CirrusCI(),
#        TravisCI(),
#        GitHubPages(),
#    ]
# )

include("secrets.jl")
