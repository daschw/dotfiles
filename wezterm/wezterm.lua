local wezterm = require("wezterm")
local font = wezterm.font("JuliaNerd")

return {
    -- default shell
    default_prog = { "nu" },
    -- fonts
    font = font,
    font_size = 12.0,
    -- enable_scroll_bar = true,
    scrollback_lines = 3500,
    window_close_confirmation = "NeverPrompt",
    -- tabs
    use_fancy_tab_bar = true,
    hide_tab_bar_if_only_one_tab = true,
    window_frame = { font = font },
    -- theme
    colors = require("theme"),
    window_padding = {
        left = "2cell",
        right = "2cell",
        top = "1cell",
        bottom = "1cell",
    },
    -- window
    adjust_window_size_when_changing_font_size = false,
}
