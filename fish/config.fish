if status is-interactive
    # Commands to run in interactive sessions can go here
end

zoxide init fish | source
starship init fish | source

source ~/.bash_aliases

# leaf themes
source "$(status dirname)/theme.fish"
# source ~/Projects/leaf.nvim/extras/fish/leaf-light.fish
# source ~/Projects/leaf.nvim/extras/fish/leaf-lighter.fish
# source ~/Projects/leaf.nvim/extras/fish/leaf-lightest.fish
# source ~/Projects/leaf.nvim/extras/fish/leaf-dark.fish
# source ~/Projects/leaf.nvim/extras/fish/leaf-darker.fish
# source ~/Projects/leaf.nvim/extras/fish/leaf-darkest.fish

function fish_greeting
    set fish_dir (status dirname)
    set cats_dir "$fish_dir/cats"
    # set fn (random choice (exa $cats_dir))
    set fn "sleeping.txt"
    set_color (random choice "red" "green" "yellow" "blue" "purple")
    # set_color "green"
    echo ""
    cat "$cats_dir/$fn"
    echo ""
    fortune
end

# use vi-like key bindings in fish
# (https://fishshell.com/docs/current/interactive.html#vi-mode-commands)
function fish_user_key_bindings
    # Execute this once per mode that emacs bindings should be used in
    fish_default_key_bindings -M insert

    # Then execute the vi-bindings so they take precedence when there's a conflict.
    # Without --no-erase fish_vi_key_bindings will default to
    # resetting all bindings.
    # The argument specifies the initial mode (insert, "default" or visual).
    fish_vi_key_bindings --no-erase insert
end
fish_user_key_bindings
