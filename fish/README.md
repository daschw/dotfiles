# fish

This config uses my [leaf](https://github.com/daschw/leaf.nvim) colorscheme.
The ASCII arts used for the greeting are stored in the [cats](fish/cats) folder and taken from [https://www.asciiart.eu/animals/cats](https://www.asciiart.eu/animals/cats).
