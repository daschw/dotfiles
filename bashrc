#!/usr/bin/env bash

# set the default editor
export EDITOR="nvim"

# theme
THEME="$(cat /home/dani/dotfiles/theme.conf)"
export THEME

# export ssh socket
export SSH_AUTH_SOCK="$XDG_RUNTIME_DIR/ssh-agent.socket"

# set Gurobi environment variables
# export GUROBI_HOME="/opt/gurobi911/linux64"
# export GUROBI_HOME="/opt/gurobi912/linux64"
export GUROBI_HOME="/opt/gurobi950/linux64"
export PATH="${PATH}:${GUROBI_HOME}/bin"
export LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:${GUROBI_HOME}/lib" 

# use native file dialog in firefox (this does not seem to work)
# export MOZ_PLUGIN_PATH="/usr/lib/mozilla/plugins"
# export GTK_USE_PORTAL=1

# matplotlib backend
# export MPLBACKEND=QT5Agg

# fix GLMakie.jl
# see https://github.com/JuliaGL/GLFW.jl/issues/198#issuecomment-740124490
export LD_PRELOAD=/usr/lib/libstdc++.so.6

# julia
export JULIA_NUM_THREADS=8

# add user local bin directory to path (for gruvbox-factory)
export PATH=/home/dani/.local/bin:$PATH
# add user local cargo bin directory to path (for rust projects)
export PATH=/home/dani/.cargo/bin:$PATH

# haskell tools with ghcup
export PATH="$HOME/.cabal/bin:$HOME/.ghcup/bin:$PATH"
# export PATH="$HOME/.ghcup/ghc/8.10.7/bin/:$PATH"
# export PATH="$(ghcup whereis ghc -d):$PATH"
# export PATH="$HOME/.ghcup/ghc/:$PATH"

# spotify client id for spotifyd
export SPOTIFYD_CLIENT_ID="$(pass accounts/spotify | grep client_id | awk '{print $2}')"

# support nvim colors in tmux
# alias tmux="TERM=screen-256color-bce tmux"
export TERM=xterm-256color

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# custom command prompt
PS1='[\u@\h \W]\$ '

# start fish
if [[ $(ps --no-header --pid=$PPID --format=cmd) != "fish" && -z ${BASH_EXECUTION_STRING} ]]
then
	exec fish
fi

# starship
eval "$(starship init bash)"

source /home/dani/.config/broot/launcher/bash/br
