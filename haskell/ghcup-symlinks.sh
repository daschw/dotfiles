#!/usr/bin/env bash

sudo ln -s "$(ghcup whereis ghc -d)/ghc" /usr/bin/ghc
sudo ln -s "$(ghcup whereis ghc -d)/ghc-pkg" /usr/bin/ghc-pkg
sudo ln -s "$(ghcup whereis ghc -d)/ghci" /usr/bin/ghci
sudo ln -s "$(ghcup whereis ghc -d)/haddock" /usr/bin/haddock
sudo ln -s "$(ghcup whereis ghc -d)/runghc" /usr/bin/runghc
sudo ln -s "$(ghcup whereis ghc -d)/runhaskell" /usr/bin/runhaskell

sudo ln -s "$(ghcup whereis hls)" /usr/bin/haskell-language-server-wrapper
sudo ln -s "$(ghcup whereis hls)" /usr/bin/hls
sudo ln -s "$(ghcup whereis cabal)" /usr/bin/cabal
